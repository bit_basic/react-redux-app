import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';


import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import * as reducers from './reducers';
//init state https://redux.js.org/recipes/structuring-reducers/initializing-state
const store = createStore(combineReducers(reducers), {data: {visible: 1}});

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
registerServiceWorker();
