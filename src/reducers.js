import { v4 } from 'uuid';

//https://stackoverflow.com/a/10727155
const randomString = (length, chars) => {
    let mask = '';
    if (chars.indexOf('a') > -1) mask += 'abcdefghijklmnopqrstuvwxyz';
    if (chars.indexOf('A') > -1) mask += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    if (chars.indexOf('#') > -1) mask += '0123456789';
    if (chars.indexOf('!') > -1) mask += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\';
    let result = '';
    for (let i = length; i > 0; --i) result += mask[Math.floor(Math.random() * mask.length)];
    return result;
}

export const data = (state, action) => {
    let newState = { visible: 1 };
    switch (action.type) {
        case 'START_EXERCIZE':
            state.id = v4();
            state.visible = action.visible;//2
            // console.log("action fire");
            // console.log(state);
            return Object.assign({ ...state });


        case 'SET_TIME':
            state.id = v4();
            state.seconds = action.seconds;
            // console.log("set_time fire");
            // console.log(state);
            return Object.assign({ ...state });

 
        case 'SET_SYMBOLS':
            //полезно https://redux.js.org/faq/react-redux#react-not-rerendering
            state.id = v4();
            state.symbols = action.symbols;
            state.symbolsString = randomString(action.symbols, '#a');
            // console.log("set_symbols fire");
            // console.log(state);
            return Object.assign({ ...state });

        case 'SET_ANSWER':
            state.id = v4();
            state.answer = action.answer;
            // console.log("set_answer fire");
            // console.log(state);
            return Object.assign({ ...state });

        case 'CHECK_ANSWER':
            state.id = v4();
            state.visible = action.visible; //4
            state.correct = (state.symbolsString === state.answer ? true : false);
            // console.log("check_answer fire");
            // console.log(state);
            return Object.assign({ ...state });

        case 'RESTART':
            state.id = v4();
            state.visible = action.visible; //1
            state.seconds = undefined;
            state.symbols = undefined;
            state.answer = undefined;
            state.symbolsString = undefined;
            // console.log("restart fire");
            // console.log(state);
            return Object.assign({ ...state });

        case 'HIDE_SYMBOLS':
            state.id = v4();
            state.visible = action.visible;//3
            // console.log("hideSymbols fire");
            // console.log(state);
            return Object.assign({ ...state });

        case 'SHOW_MESSAGE':
            state.id = v4();
            state.message = action.message;
            state.messageOpen = true;
            // console.log("showMessage fire");
            // console.log(state);
            return Object.assign({ ...state });

        default:
            return newState;
    }
};