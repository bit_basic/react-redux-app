import React, { Component } from 'react';
import './App.css';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { connect } from 'react-redux';
import { startEx, setTime, setSymbols, setAnswer, checkAnswer, restart, hideSymbols, showMessage } from './actions';
import Modal from '@material-ui/core/Modal';




//чтение состояния
const mapStateToProps = (state) => {
  return { state: state }

};
//передача события
const mapDispatchToProps = (dispatch) => {
  return {
    startEx: () => {
      dispatch(startEx());

    },
    setTime: (seconds) => {
      dispatch(setTime(seconds));
    },
    setSymbols: (symbols) => {
      dispatch(setSymbols(symbols));
    },
    setAnswer: (answer) => {
      dispatch(setAnswer(answer));
    },
    checkAnswer: () => {
      dispatch(checkAnswer());
    },
    restart: () => {
      dispatch(restart());
    },
    hideSymbols: () => {
      dispatch(hideSymbols());
    },
    showMessage: (message) => {
      dispatch(showMessage(message));
    }
  }
};




const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  menu: {
    width: 200,
  },
});


class App extends Component {


  startEx = () => {
    if (!this.props.state.data.seconds) {
      console.log("Введите секунды");
      return;
    }
    let sec = this.props.state.data.seconds * 1000;
    sec = +sec;
    //console.log("sec ", sec);
    if (!this.props.state.data.symbols) {
      console.log("Введите количество символов");
      return;
    }
    this.props.startEx();
    setTimeout(() => {
      this.props.hideSymbols()
    }, sec);

  };

  setTime = (e) => {
    this.props.setTime(e.target.value);
  }

  setSymbols = (e) => {
    this.props.setSymbols(e.target.value);
    //console.log(this.props);
  }

  setAnswer = (e) => {
    this.props.setAnswer(e.target.value);
    //console.log(this.props);
  }

  checkAnswer = () => {
    this.props.checkAnswer();
  };

  restart = () => {
    this.props.restart();

  };

  hideSymbols = () => {
    this.props.hideSymbols();
  };




  render() {
    return (

      <div className="App">

        <form className={this.props.state.data.visible === 1 ? '' : 'hidden'} noValidate autoComplete="off">

          <TextField

            id="seconds"
            label="секунды?"
            placeholder="1-10"
            className={styles.textField}
            margin="normal"
            onChange={this.setTime}
            required
            value={this.props.state.data.seconds || ''}


          />
          <br />

          <TextField
            id="symbols"
            label="сколько символов?"
            placeholder="1-10"
            className={styles.textField}
            margin="normal"
            onChange={this.setSymbols}
            required
            value={this.props.state.data.symbols || ''}

          />
          <br />

          <Button variant="raised" color="primary" onClick={this.startEx}>
            Старт!
          </Button>

        </form>


        <h1 className={this.props.state.data.visible === 2 ? '' : 'hidden'}>{this.props.state.data.symbolsString || "Символы"}</h1>


        <form className={this.props.state.data.visible === 3 ? '' : 'hidden'} noValidate autoComplete="off">
          <TextField
            id="answer"
            label="ваш ответ?"

            margin="normal"
            onChange={this.setAnswer}
            value={this.props.state.data.answer || ''}
          />


          <Button variant="raised" color="primary" onClick={this.checkAnswer}>
            Проверить
          </Button>

        </form>



        <div className={this.props.state.data.visible === 4 ? '' : 'hidden'}>
          <p>Ваш ответ: {this.props.state.data.answer}</p>
          <p>Правильный ответ: {this.props.state.data.symbolsString}</p>
          <strong>Вы ответили {this.props.state.data.answer === this.props.state.data.symbolsString ? "верно" : "неверно"}</strong>
          <br />
          <Button variant="raised" color="primary" onClick={this.restart}>
            Еще одна попытка
          </Button>
        </div>


        <Modal
          open={this.props.state.data.messageOpen || false}
          onClose={this.handleClose}
        >
          <div>
            {this.props.state.data.message}
          </div>
        </Modal>


      </div>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);