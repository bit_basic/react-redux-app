export const startEx = () => ({
    type: 'START_EXERCIZE',
    visible: 2
  });

  export const setTime = seconds => ({
      type: 'SET_TIME',
      seconds: seconds
  });

  export const setSymbols = symbols => ({
    type: 'SET_SYMBOLS',
    symbols: symbols
});

export const setAnswer = answer => ({
    type: 'SET_ANSWER',
    answer: answer
});

export const checkAnswer = data => ({
    type: 'CHECK_ANSWER',
    visible: 4
});

export const restart = () => ({
    type: 'RESTART',
    visible: 1
});

export const hideSymbols = () => ({
    type: 'HIDE_SYMBOLS',
    visible: 3
});

export const showMessage = message => ({
    type: 'SHOW_MESSAGE',
    message: message
});